#----------------------------------------------------------------------------
OUT_NAME    := geocalc
OUT_DIR     := .
CLEAN_FILES := "$(OUT_DIR)/$(OUT_NAME).exe" a.out
#----------------------------------------------------------------------------
# 1-st way to select source files
SRCS := \
        fly_lib/fly_lib.c \
        geocalc.c \
        geocalc_main.c
        
HDRS := \
        fly_lib/fly_lib.c \
        geocalc.c

# 2-nd way to select source files
#SRC_DIRS := . ../libs
#HDR_DIRS := . ../libs
#----------------------------------------------------------------------------
DEFS    := 
#OPTIM  := -g -O0
OPTIM   := -Os
WARN    := -Wall -Wno-pointer-to-int-cast
CFLAGS  := $(WARN) $(OPTIM) $(DEFS) $(CFLAGS) -pipe
LDFLAGS := -lm $(LDFLAGS)
PREFIX  := /usr/local
#----------------------------------------------------------------------------
#_AS  := @as
#_CC  := @gcc
#_CXX := @g++
#_LD  := @gcc

#_CC  := @clang
#_CXX := @clang++
#_LD  := @clang
#----------------------------------------------------------------------------
#DEPS_DIR := ../.dep
#OBJS_DIR := ../.obj
#----------------------------------------------------------------------------
include Makefile.skel
#----------------------------------------------------------------------------

