#! /bin/bash

echo
echo "--------------"
echo "северный полюс"
echo "--------------"
./geocalc -v -to-geoc -B 90 -L 0 -H 0
./geocalc -v -to-geog -X 0.000 -Y 0.000 -Z 6356863.019

echo
echo "----------------"
echo "гринвич, экватор"
echo "----------------"
./geocalc -v -to-geoc -B 0 -L 0 -H 0
./geocalc -v -to-geog -X 6378245.000 -Y 0.000 -Z 0.000


echo
echo "---------"
echo "широта 45"
echo "---------"
./geocalc -v -to-geoc -B 45 -L 0 -H 0
./geocalc -v -to-geog -X 4517666.288 -Y 0.000 -Z 4487427.644
./geocalc -v -to-geoc -B 45 -L 30 -H 150
./geocalc -v -to-geog -X 3912505.627 -Y 2258886.177 -Z 4487533.710


echo
echo "---------"
echo "широта 60"
echo "---------"
./geocalc -v -to-geoc -B 60 -L 0 -H 0
./geocalc -v -to-geog -X 3197157.568 -Y 0.000 -Z 5500573.593
./geocalc -v -to-geoc -B 60 -L -89 -H 10000
./geocalc -v -to-geog -X 55885.355 -Y -3201669.864 -Z 5509233.847

echo
echo "-------------"
echo "Moscow oblast"
echo "-------------"
./geocalc -v -to-geoc -B 55.5673243409 -L 38.0612467782 -H 119.647
./geocalc -v -to-geog -X 2846124.979 -Y 2228540.118 -Z 5237544.296
./geocalc -v -to-geoc -B 55.5673243407 -L 38.0612467791 -H 119.647

./geocalc -v -wgs -to-geog -X 2846148.2753 -Y 2228409.5787 -Z 5237458.5137
./geocalc -v --WGS84 -to-geoc -B 55.5673716664 -L 38.0593900001 -H 123.600


