/*
 * geocalc - геокалькулятор
 * Файл: "geocalc.c" (основной модуль консольного приложения)
 */

//----------------------------------------------------------------------------
#include "geocalc.h"
#include "fly_lib.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//-----------------------------------------------------------------------------
// координаты по умолчанию
#define GEOCALC_L0 40.3260810 // долгота [град]
#define GEOCALC_B0 62.7613930 // широта [град]
#define GEOCALC_H0 4.         // высота [м]

#define GEOCALC_EG 55.0       // азимутальный угол [град]
#define GEOCALC_EV 45.        // угол места [гра]
#define GEOCALC_R  15.        // дальность [м]

#define GEOCALC_L  40.3304662 // долгота [град]
#define GEOCALC_B  62.7584789 // широта [град]
#define GEOCALC_H  15.        // высота [м]
//-----------------------------------------------------------------------------
// command line options
typedef struct options_ {
  int verbose; // verbose level {0,1,2,3}
  int mode;    // mode {0:direct, 1:reverse, 2:to-geoc, 3:to-geog}
  char sk[10]; // "SK-42" or "WGS84"
  
  double B0; // широта точки начала координта [град]
  double L0; // долгота точки начала координта [град]
  double H0; // высота точики начала координат [м]
  
  double B;  // широта точки визирования [град]
  double L;  // долгота точки визирования [град]
  double H;  // высота точики визирования [м]

  double Eg; // азимутальный угол точки визирования [град]
  double Ev; // угол места точки визирования [град]
  double R;  // радиус вектор (дальность) до точки визирования [м]

  double x; // X 
  double y; // Y - координаты в геоцентрической СК
  double z; // Z 
} options_t;
//----------------------------------------------------------------------------
static void usage()
{
  fprintf(stderr,
    "Geo calculator\n"
    "Usage: geocalc [-options]\n"
    "       geocalc --help\n");
  exit(EXIT_FAILURE);
}
//---------------------------------------------------------------------------
static void help()
{
  printf(
    "Geo calculator\n"
    "Usage: geocalc [-options]\n"
    "Options:\n"
    "    -h|--help    - show this help\n"
    "    -v|--verbose - verbose output\n"
    "    -wgs|--WGS84 - use WGS84 (experemental\n"
    "    -sk42|--SK42 - use SK-42 (by default\n"
    "    -r|--reverse - reverse mode (calculate B/L/H)\n"
    "    -to-geoc     - convert B/L/H то geocentic x/y/z\n"
    "    -to-geog     - convert geocentric x/y/z to B/L/H\n"
    "    -B0|-b0 B    - base latitude [deg]\n"
    "    -L0|-l0 L    - base longtitude [deg]\n"
    "    -H0|-h0 H    - base altitude [m]\n"
    "    -Eg|-eg Eg   - azimuth angle [deg]\n"
    "    -Ev|-ev Ev   - elevation angle [deg]\n"
    "    -R|-d R      - distanse [m]\n"
    "    -B|-b B      - latitude [deg]\n"
    "    -L|-l L      - longtitude [deg]\n"
    "    -H|-a H      - altitude [m]\n"
    "    -X|-x X      - \\\n"
    "    -Y|-y Y      - - geocentric coordinates\n"
    "    -Z|-z Z      - /\n"
    );
  exit(EXIT_SUCCESS);
}
//-----------------------------------------------------------------------------
// parse command options
static void parse_options(int argc, const char *argv[], options_t *o)
{
  int i;
  
  // set options by default
  o->verbose = 0;
  o->mode = 0;
  strcpy(o->sk, "SK-42");
  o->B0 = GEOCALC_B0;
  o->L0 = GEOCALC_L0;
  o->H0 = GEOCALC_H0;
  o->B  = GEOCALC_B;
  o->L  = GEOCALC_L;
  o->H  = GEOCALC_H;
  o->Eg = GEOCALC_EG;
  o->Ev = GEOCALC_EV;
  o->R  = GEOCALC_R;
  
  // pase options
  for (i = 1; i < argc; i++)
  {
    if (argv[i][0] == '-')
    { // parse options
      if (!strcmp(argv[i], "-h") ||
          !strcmp(argv[i], "--help"))
      { // print help
        help();
      }
      else if (!strcmp(argv[i], "-v") ||
               !strcmp(argv[i], "--verbose"))
      {
        o->verbose = 1;
      }
      else if (!strcmp(argv[i], "-wgs") ||
               !strcmp(argv[i], "--WGS84"))
      {
        fly_set_wgs84();
	strcpy(o->sk, "WGS84");
      }
      else if (!strcmp(argv[i], "-sk42") ||
               !strcmp(argv[i], "--SK42"))
      {
        fly_set_sk42();
	strcpy(o->sk, "SK-42");
      }
      else if (!strcmp(argv[i], "-r") ||
               !strcmp(argv[i], "--reverse"))
      {
        o->mode = 1;
      }
      else if (!strcmp(argv[i], "-to-geoc"))
      {
        o->mode = 2;
      }
      else if (!strcmp(argv[i], "-to-geog"))
      {
        o->mode = 3;
      }
      else if (!strcmp(argv[i], "-t") ||
               !strcmp(argv[i], "--test"))
      {
        o->mode = 4;
      }
      else if (!strcmp(argv[i], "-b0") ||
               !strcmp(argv[i], "-B0"))
      {
        if (++i >= argc) usage();
        o->B0 = atof(argv[i]);
      }
      else if (!strcmp(argv[i], "-l0") ||
               !strcmp(argv[i], "-L0"))
      {
        if (++i >= argc) usage();
        o->L0 = atof(argv[i]);
      }
      else if (!strcmp(argv[i], "-h0") ||
               !strcmp(argv[i], "-H0"))
      {
        if (++i >= argc) usage();
        o->H0 = atof(argv[i]);
      }
      else if (!strcmp(argv[i], "-b") ||
               !strcmp(argv[i], "-B"))
      {
        if (++i >= argc) usage();
        o->B = atof(argv[i]);
      }
      else if (!strcmp(argv[i], "-l") ||
               !strcmp(argv[i], "-L"))
      {
        if (++i >= argc) usage();
        o->L = atof(argv[i]);
      }
      else if (!strcmp(argv[i], "-a") ||
               !strcmp(argv[i], "-H"))
      {
        if (++i >= argc) usage();
        o->H = atof(argv[i]);
      }
      else if (!strcmp(argv[i], "-eg") ||
               !strcmp(argv[i], "-Eg"))
      {
        if (++i >= argc) usage();
        o->Eg = atof(argv[i]);
      }
      else if (!strcmp(argv[i], "-ev") ||
               !strcmp(argv[i], "-Ev"))
      {
        if (++i >= argc) usage();
        o->Ev = atof(argv[i]);
      }
      else if (!strcmp(argv[i], "-R") ||
               !strcmp(argv[i], "-d"))
      {
        if (++i >= argc) usage();
        o->R = atof(argv[i]);
      }
      else if (!strcmp(argv[i], "-X") ||
               !strcmp(argv[i], "-x"))
      {
        if (++i >= argc) usage();
        o->x = atof(argv[i]);
      }
      else if (!strcmp(argv[i], "-Y") ||
               !strcmp(argv[i], "-y"))
      {
        if (++i >= argc) usage();
        o->y = atof(argv[i]);
      }
      else if (!strcmp(argv[i], "-Z") ||
               !strcmp(argv[i], "-z"))
      {
        if (++i >= argc) usage();
        o->z = atof(argv[i]);
      }
      else // unknown option
        usage();
    } // if (argv[i][0] == '-')
    else 
    { // other arguments whithout '-'
      //o->other = argv[i];
      usage();
    }
  } // for (i = 1; i < argc; i++)
}
//-----------------------------------------------------------------------------
// main function
int main(int argc, const char *argv[])
{
  options_t o;

  // parse command line options
  parse_options(argc, argv, &o);

  // show options
  if (o.verbose >= 1)
  {
    printf("Geo caclulator start with options:\n");
    printf("   verbose = %i\n",   o.verbose);
    printf("   sk      = '%s'\n", o.sk);
    printf("   mode    = '%s'\n",
           o.mode == 0 ? "direct"  :
           o.mode == 1 ? "reverse" :
           o.mode == 2 ? "to-geoc" :
           o.mode == 3 ? "to-geog" :
           o.mode == 4 ? "test" :
                         "do nothing");
  }

  if (o.mode == 0)
  { // "direct"
    if (o.verbose >= 1)
    {
      printf("   B0 = %.10f deg\n", o.B0);
      printf("   L0 = %.10f deg\n", o.L0);
      printf("   H0 = %.10f m\n",   o.H0);
      printf("   B  = %.10f deg\n", o.B);
      printf("   L  = %.10f deg\n", o.L);
      printf("   H  = %.3f m\n\n",  o.H);
    }
    
    geocalc_direct(o.B0, o.L0, o.H0,
                   o.B,  o.L,  o.H,
                   &o.Eg, &o.Ev, &o.R);
      
    printf("Geo caclulator results:\n");
    printf("=> Eg = %.4f deg\n", o.Eg);
    printf("=> Ev = %.4f deg\n", o.Ev);
    printf("=> R  = %.3f m\n\n", o.R);
  }
  else if (o.mode == 1)
  { // "reverse"
    if (o.verbose >= 1)
    {
      printf("   B0 = %.10f deg\n", o.B0);
      printf("   L0 = %.10f deg\n", o.L0);
      printf("   H0 = %.3f m\n",    o.H0);
      printf("   Eg = %.4f deg\n",  o.Eg);
      printf("   Ev = %.4f deg\n",  o.Ev);
      printf("   R  = %.3f m\n\n",  o.R);
    }
    
    geocalc_reverse(o.B0, o.L0, o.H0,
                    o.Eg, o.Ev, o.R,
                    &o.B, &o.L, &o.H);

    printf("Geo caclulator results:\n");
    printf("=> B  = %.10f deg\n", o.B);
    printf("=> L  = %.10f deg\n", o.L);
    printf("=> H  = %.4f m\n\n",  o.H);
  }
  else if (o.mode == 2)
  { // "-to-geoc"
    fly_geog g;
    fly_3d x;
    
    if (o.verbose >= 1)
    {
      printf("   B = %.10f deg\n", o.B);
      printf("   L = %.10f deg\n", o.L);
      printf("   H = %.4f m\n\n",  o.H);
    }

    g.fi     = o.B * FLY_DEG2RAD;
    g.lambda = o.L * FLY_DEG2RAD;
    g.h      = o.H;

    fly_geog2xo(&g, &x);

    printf("Geo caclulator results:\n");
    printf("=> X = %.4f m\n",   x.x);
    printf("=> Y = %.4f m\n",   x.y);
    printf("=> Z = %.4f m\n\n", x.z);
  }
  else if (o.mode == 3)
  { // "-to-geog"
    fly_3d x;
    fly_geog g;
    
    if (o.verbose >= 1)
    {
      printf("   X = %.4f m\n",   o.x);
      printf("   Y = %.4f m\n",   o.y);
      printf("   Z = %.4f m\n\n", o.z);
    }

    x.x = o.x;
    x.y = o.y;
    x.z = o.z;
    
    fly_xo2geog(&x, &g);
    
    printf("Geo caclulator results:\n");
    printf("=> B = %.10f deg\n", g.fi     * FLY_RAD2DEG);
    printf("=> L = %.10f deg\n", g.lambda * FLY_RAD2DEG);
    printf("=> H = %.4f m\n\n",  g.h);
  }
  else
  { // internal test
    fly_geog geog;
    fly_geoc geoc;
    fly_3d xo;
    
    geog.fi     = 45. * FLY_DEG2RAD;
    geog.lambda = 50. * FLY_DEG2RAD;
    geog.h      = 5000.;

    printf("Geo internal test:\n");
    printf("   B = %.10f deg\n", geog.fi     * FLY_RAD2DEG);
    printf("   L = %.10f deg\n", geog.lambda * FLY_RAD2DEG);
    printf("   H = %.4f m\n\n",  geog.h);
    fly_geog2xo(&geog, &xo);
    printf("   X = %.4f m\n",   xo.x);
    printf("   Y = %.4f m\n",   xo.y);
    printf("   Z = %.4f m\n\n", xo.z);

    fly_xo2geoc(&xo, &geoc);
    printf("=> geoc.fi - geoc.fi = %.10f'\n\n",
           (geog.fi - geoc.fi) * FLY_RAD2DEG * 60.);
    
    fly_xo2geog(&xo, &geog);
    printf("=> B = %.10f deg\n", geog.fi     * FLY_RAD2DEG);
    printf("=> L = %.10f deg\n", geog.lambda * FLY_RAD2DEG);
    printf("=> H = %.4f m\n\n",  geog.h);

    fly_geog2xo(&geog, &xo);
    printf("=> X = %.4f m\n",   xo.x);
    printf("=> Y = %.4f m\n",   xo.y);
    printf("=> Z = %.4f m\n\n", xo.z);
  }

  return EXIT_SUCCESS;
}
//----------------------------------------------------------------------------

/*** end of "geocalc_main.c" ***/


