﻿/*
 * Набор функций для работы с векторами и матрицами для навигационных расчетов
 * Файл: "fly_lib.c"
 * Дата последней модификации: 2021.03.14
 */

//---------------------------------------------------------------------------
#include <stdlib.h>
#include "fly_lib.h"
//---------------------------------------------------------------------------
// радиус референц-эллипсоида на экваторе [м]
static double fly_az = FLY_AZ_WGS84;

// радиус референц-эллипсоида на полюсах [м]
static double fly_bz = FLY_BZ_WGS84;
//---------------------------------------------------------------------------
// создание единичного (нормированного) кватерниона
void fly_quat_make(
  const fly_3d *n, // ось вращения (не обязательно нормированная)
  double teta,     // угол вращения [рад]
  fly_quat *q)     // инициализируемый кватернион
{
  double t2 = teta * 0.5;
  double s = sin(t2) / FLY_MODULE(n->x, n->y, n->z);
  q->x = n->x * s;
  q->y = n->y * s;
  q->z = n->z * s;
  q->w = cos(t2);
}
//---------------------------------------------------------------------------
// функция перевода (единичного) кватерниона в ось и угол
void fly_quat2axis(const fly_quat *q, fly_3d *n, double *teta)
{
  double v = FLY_MODULE(q->x, q->y, q->z);
  if (v > 0.)
  {
    double norm = 1. / v;
    n->x = q->x * norm;
    n->y = q->y * norm;
    n->z = q->z * norm;
    if (q->w >= 0.) *teta = 2. * atan2(v,   q->w); // 0..+Pi
    else            *teta = 2. * atan2(-v, -q->w); // -Pi..0
  }
  else
  {
    n->x = n->y = n->z = 0.;
    *teta = 0.;
  }
}
//---------------------------------------------------------------------------
// вычислить сопряженный кватернион
void fly_quat_conj(const fly_quat *src, fly_quat *dst)
{
  dst->x = - src->x;
  dst->y = - src->y;
  dst->z = - src->z;
  dst->w =   src->w;
}
//---------------------------------------------------------------------------
// вычислить обратный кватернион
void fly_quat_inv(const fly_quat *src, fly_quat *dst)
{
  fly_quat_conj(src, dst);
  double a2 = FLY_QUAT_MODULE2(dst->x, dst->y, dst->z, dst->w);
  if (a2 > 0.)
  {
    dst->x /= a2;
    dst->y /= a2;
    dst->z /= a2;
    dst->w /= a2;
  }
}
//---------------------------------------------------------------------------
// функция привидения модуля вектора к единице
// (предполагаем, что вектор не нулевой)
void fly_vector_norm(fly_3d *v)
{
  double a = FLY_MODULE(v->x, v->y, v->z); 
  if (a == 0.) return; // нулевой вектор - ОШИБКА
  v->x /= a;
  v->y /= a;
  v->z /= a;
}
//---------------------------------------------------------------------------
void fly_vector_norm_copy(const fly_3d *src, fly_3d *dst)
{
  fly_vector_copy(src, dst);
  fly_vector_norm(dst);
}
//---------------------------------------------------------------------------
// функция нормализации кватерниона (привидение длины к единице)
// (предполагаем, что кватернион не нулевой)
void fly_quat_norm(fly_quat *q)
{
  double a = FLY_QUAT_MODULE(q->x, q->y, q->z, q->w);
  if (a == 0.) return; // нулевой кватернион - ОШИБКА
  q->x /= a;
  q->y /= a;
  q->z /= a;
  q->w /= a;
}
//---------------------------------------------------------------------------
void fly_quat_norm_copy(const fly_quat *src, fly_quat *dst)
{
  fly_quat_copy(src, dst);
  fly_quat_norm(dst);
}
//---------------------------------------------------------------------------
// функция вычисления векторного произведения в Декартовой СК C = [A x B]
// (указатель на вектор C может совпадать с указателем на вектор A или B)
void fly_vector_mpy(const fly_3d *A, const fly_3d *B, fly_3d *C)
{
  double x, y;
  x    = A->y * B->z - A->z * B->y;
  y    = A->z * B->x - A->x * B->z;
  C->z = A->x * B->y - A->y * B->x;
  C->x = x;
  C->y = y;
}
//---------------------------------------------------------------------------
// функция умножения кватернионов C = A x B
// (указатель на кватернион C может совпадать с
// указателем на кватернион A или B)
void fly_quat_mpy(const fly_quat *A, const fly_quat *B, fly_quat *C)
{
#if 0 // "медленный" вариант
  fly_3d v; // v - векторное произведение векторов направлений A и B
  v.x = A->y * B->z - A->z * B->y;
  v.y = A->z * B->x - A->x * B->z;
  v.z = A->x * B->y - A->y * B->x;

  v.x += A->w * B->x + B->w * A->x;
  v.y += A->w * B->y + B->w * A->y;
  v.z += A->w * B->z + B->w * A->z;

  C->w = A->w * B->w - (A->x * B->x + A->y * B->y + A->z * B->z);
  C->x = v.x;
  C->y = v.y;
  C->z = v.z;
#else // "быстрый" вариант
  double a = (A->w + A->x) * (B->w + B->x);
  double b = (A->z - A->y) * (B->y - B->z);
  double c = (A->x - A->w) * (B->y + B->z);
  double d = (A->y + A->z) * (B->x - B->w);

  double e = (A->x + A->z) * (B->x + B->y) * 0.5;
  double f = (A->x - A->z) * (B->x - B->y) * 0.5;
  double g = (A->w + A->y) * (B->w - B->z) * 0.5;
  double h = (A->w - A->y) * (B->w + B->z) * 0.5;

  C->x =  a - e - f - g - h;
  C->y = -c + e - f + g - h;
  C->z = -d + e - f - g + h;
  C->w =  b - e - f + g + h;
#endif
}
//---------------------------------------------------------------------------
// функция умножения кватерниона на вектор C = A x B
// (A - кватернион, B - вектор, указатель на кватернион C
// может совпадать с указателем на кватернион A)
void fly_quat_vector_mpy(const fly_quat *A, const fly_3d *B, fly_quat *C)
{
  double a = (A->w + A->x) *  B->x;
  double b = (A->z - A->y) * (B->y - B->z);
  double c = (A->x - A->w) * (B->y + B->z);
  double d = (A->y + A->z) *  B->x;

  double e = (A->x + A->z) * (B->x + B->y) * 0.5;
  double f = (A->x - A->z) * (B->x - B->y) * 0.5;
  double h = B->z * -0.5;
  double g = (A->w + A->y) * h;
  h       *= (A->y - A->w);

  C->x =  a - e - f - g - h;
  C->y = -c + e - f + g - h;
  C->z = -d + e - f - g + h;
  C->w =  b - e - f + g + h;
}
//---------------------------------------------------------------------------
// функция умножения вектора на кватернион C = A x B
// (A - вектор, B - кватернион, указатель на кватернион C
// может совпадать с указателем на кватернион B)
void fly_vector_quat_mpy(const fly_3d *A, const fly_quat *B, fly_quat *C)
{
  double a =  A->x         * (B->w + B->x);
  double b = (A->z - A->y) * (B->y - B->z);
  double c =  A->x         * (B->y + B->z);
  double d = (A->y + A->z) * (B->x - B->w);

  double e = (A->x + A->z) * (B->x + B->y) * 0.5;
  double f = (A->x - A->z) * (B->x - B->y) * 0.5;
  double h =  A->y * -0.5;
  double g = (B->z - B->w) * h;
  h       *= (B->w + B->z);

  C->x =  a - e - f - g - h;
  C->y = -c + e - f + g - h;
  C->z = -d + e - f - g + h;
  C->w =  b - e - f + g + h;
}
//---------------------------------------------------------------------------
// функция деления кватернионов C = A / B
// (указатель на кватернион C может совпадать с
// указателем на кватернион A или B)
void fly_quat_div(const fly_quat *A, const fly_quat *B, fly_quat *C)
{
  fly_quat b;
  fly_quat_inv(B, &b);
  fly_quat_mpy(A, &b, C);
}
//---------------------------------------------------------------------------
// функция перевода (единичного!) кватерниона в матрицу вращения
void fly_quat2m(const fly_quat *q, fly_3x3 *m)
{
  // если q = [ x y z w ], то:
  //                               2     2
  //     [ i.x j.x k.x ]   [ 1 - 2y - 2z    2xy - 2zw      2xz + 2yw     ]
  //     [             ]   [                      2     2                ]
  // m = [ i.y j.y k.y ] = [ 2xy + 2zw      1 - 2x - 2z    2yz - 2xw     ] 
  //     [             ]   [                                     2     2 ]
  //     [ i.z j.z k.z ]   [ 2xz - 2yw      2yz + 2xw      1 - 2x - 2y   ]
  double x2 = q->x + q->x; // 2x 
  double y2 = q->y + q->y; // 2y
  double z2 = q->z + q->z; // 2z
  double xx = q->x * x2;   // 2x**2
  double yy = q->y * y2;   // 2y**2
  double zz = q->z * z2;   // 2z**2
  double xy = q->x * y2;   // 2xy
  double xz = q->x * z2;   // 2xz
  double yz = q->y * z2;   // 2yz
  double xw = q->w * x2;   // 2xw
  double yw = q->w * y2;   // 2yw
  double zw = q->w * z2;   // 2zw
  m->i.x = 1. - (yy + zz);
  m->j.y = 1. - (xx + zz);
  m->k.z = 1. - (xx + yy);
  m->i.y = xy + zw;
  m->j.x = xy - zw;
  m->k.x = xz + yw;
  m->i.z = xz - yw;
  m->k.y = yz - xw;
  m->j.z = yz + xw;
}
//---------------------------------------------------------------------------
// функция перевода матрицы вращения в кватернион
void fly_m2quat(const fly_3x3 *m, fly_quat *q)
{
  double tr = m->i.x + m->j.y + m->k.z; // trace of matrix

  if (tr > 0.)
  { // if trace positive than "w" is biggest component
    double w = 1. + tr;
    double s = 0.5 / sqrt(w); // "w" contain the "norm * 4"
    q->w = s * w;
    q->x = s * (m->j.z - m->k.y);
    q->y = s * (m->k.x - m->i.z);
    q->z = s * (m->i.y - m->j.x);
  }
  else // tr <= 0. 
  { // some of vector components is bigger
    if (m->i.x >= m->j.y && m->i.x >= m->k.z)
    { // максимум в колонке i:
      double x = 1. + m->i.x - m->j.y - m->k.z;
      double s = 0.5 / sqrt(x); // "x" contain the "norm * 4"
      q->x = s * x;
      q->y = s * (m->j.x + m->i.y);
      q->z = s * (m->k.x + m->i.z);
      q->w = s * (m->j.z - m->k.y);
    }
    else if (m->j.y >= m->k.z /*&& m->j.y >= m->i.x*/)
    { // максимум в колонке j:
      double y = 1. + m->j.y - m->i.x - m->k.z;
      double s = 0.5 / sqrt(y); // "y" contain the "norm * 4"
      q->x = s * (m->j.x + m->i.y);
      q->y = s * y;
      q->z = s * (m->k.y + m->j.z);
      q->w = s * (m->k.x - m->i.z);
    }
    else // m->k.z >= m->i.x && m->k.z >= m->j.y)
    { // максимум в колонке k:
      double z = 1. + m->k.z - m->i.x - m->j.y;
      double s = 0.5 / sqrt(z); // "z" contain the "norm * 4"
      q->x = s * (m->k.x + m->i.z);
      q->y = s * (m->k.y + m->j.z);
      q->z = s * z;
      q->w = s * (m->i.y - m->j.x);
    }
  }
}
//---------------------------------------------------------------------------
// функция расчета Эйлеровых углов из (единичного!) кватерниона
void fly_quat2e(const fly_quat *q, fly_angles *e)
{
#if 0 // медленый вариант
  fly_3x3 m;
  fly_quat2m(q, &m);
  fly_mgs2e(&m, e);
#else // быстрый вариант
  double z2 = q->z * q->z;
  double sin_teta = 2. * (q->x * q->y - q->w * q->z);
  if      (sin_teta >=  1.) e->teta =  M_PI / 2.;
  else if (sin_teta <= -1.) e->teta = -M_PI / 2.;
  else                      e->teta = asin(sin_teta);
  e->gama = atan2(- q->w * q->x - q->y * q->z,
                  0.5 - q->x * q->x - z2);
  e->psi = atan2(q->w * q->y + q->x * q->z,
                 0.5 - z2 - q->y * q->y);
#endif
}
//---------------------------------------------------------------------------
// функция расчета кватерниона из Эйлеровых углов
void fly_e2quat(const fly_angles *e, fly_quat *q)
{
#if 0 // медленый вариант
  fly_3x3 m;
  fly_e2mgs(e, &m);
  fly_m2quat(&m, q);
#else // быстрый вариант
  double half_psi  = 0.5 * e->psi;
  double half_teta = 0.5 * e->teta;
  double half_gama = 0.5 * e->gama;
  double cos_psi  = cos(half_psi);
  double sin_psi  = sin(half_psi);
  double cos_teta = cos(half_teta);
  double sin_teta = sin(half_teta);
  double cos_gama = cos(half_gama);
  double sin_gama = sin(half_gama);
  double cos_gama_x_cos_teta = cos_gama * cos_teta;
  double sin_gama_x_sin_teta = sin_gama * sin_teta;
  double cos_gama_x_sin_teta = cos_gama * sin_teta;
  double sin_gama_x_cos_teta = sin_gama * cos_teta;
  q->w =   cos_gama_x_cos_teta * cos_psi + sin_gama_x_sin_teta * sin_psi;
  q->x = - sin_gama_x_cos_teta * cos_psi + cos_gama_x_sin_teta * sin_psi;
  q->y =   cos_gama_x_cos_teta * sin_psi - sin_gama_x_sin_teta * cos_psi;
  q->z = - cos_gama_x_sin_teta * cos_psi - sin_gama_x_cos_teta * sin_psi;
#endif
}
//---------------------------------------------------------------------------
// функция поворота вектора на (единичный!) кватернион
// над вектором X производится преобразование
// Y = Q x X x Q'
void fly_quat_transform(const fly_quat *Q, const fly_3d *X, fly_3d *Y)
{
#if 0
  fly_quat q, qx;
  fly_quat_conj(Q, &q);           // q = Q`
  fly_quat_vector_mpy(Q, X, &qx); // qx = Q * X
  fly_quat_mpy(&qx, &q, &q);      // q = Q * X * Q`
#else
  fly_quat q;
  fly_quat_conj(Q, &q);           // q = Q`
  fly_vector_quat_mpy(X, &q, &q); // q = X * Q`
  fly_quat_mpy(Q, &q, &q);        // q = Q * X * Q`
#endif
  Y->x = q.x;
  Y->y = q.y;
  Y->z = q.z;
}
//---------------------------------------------------------------------------
// функция перемножения матрицы 3x3 на 3-d вектор
// над вектором X производится линейное преобразование
// Y = A x X
void fly_vector_transform(const fly_3x3 *A, const fly_3d *X, fly_3d *Y)
{
  //     [ Y->x ]   [ i.x j.x k.x ]   [ X->x ]
  //     [      ]   [             ]   [      ]
  // Y = [ Y->y ] = [ i.y j.y k.y ] x [ X->y ]
  //     [      ]   [             ]   [      ]
  //     [ Y->z ]   [ i.z j.z k.z ]   [ X->z ]
  Y->x = A->i.x * X->x + A->j.x * X->y + A->k.x * X->z;
  Y->y = A->i.y * X->x + A->j.y * X->y + A->k.y * X->z;
  Y->z = A->i.z * X->x + A->j.z * X->y + A->k.z * X->z;
}
//---------------------------------------------------------------------------
// функция перемножения транспонированной матрицы 3x3 на 3-d вектор
// над вектором X производится линейное преобразование
// Y = A' x X, где X = A x Y, если матрица A производит поворот СК
void fly_vector_reverse(const fly_3x3 *A, const fly_3d *X, fly_3d *Y)
{
  //     [ Y->x ]   [ i.x i.y i.z ]   [ X->x ]
  //     [      ]   [             ]   [      ]
  // Y = [ Y->y ] = [ j.x j.y j.z ] x [ X->y ]
  //     [      ]   [             ]   [      ]
  //     [ Y->z ]   [ k.x k.y k.z ]   [ X->z ]
  Y->x = A->i.x * X->x + A->i.y * X->y + A->i.z * X->z;
  Y->y = A->j.x * X->x + A->j.y * X->y + A->j.z * X->z;
  Y->z = A->k.x * X->x + A->k.y * X->y + A->k.z * X->z;
}
//---------------------------------------------------------------------------
// поворот вектора X вокруг оси OX на угол A, результат помещается в Y
void fly_vector_rotate_ox(double A, const fly_3d *X, fly_3d *Y)
{
  double c = cos(A);
  double s = sin(A);
  double Y_y;
  
  // матрица поворота вокуруг оси OX:
  // [ 1    0       0    ]
  // [ 0  cos(A) -sin(A) ]
  // [ 0  sin(A)  cos(A) ]
  Y->x = X->x;
  Y_y  = X->y * c - X->z * s;
  Y->z = X->y * s + X->z * c;
  Y->y = Y_y;
}
//---------------------------------------------------------------------------
// поворот вектора X вокруг оси OY на угол A, результат помещается в Y
void fly_vector_rotate_oy(double A, const fly_3d *X, fly_3d *Y)
{
  double c = cos(A);
  double s = sin(A);
  double Y_x;
  
  // матрица поворота вокуруг оси OY:
  // [  cos(A)  0  sin(A) ]
  // [    0     1    0    ]
  // [ -sin(A)  0  cos(A) ]
  Y_x  = X->x * c + X->z * s;
  Y->y = X->y;
  Y->z = X->z * c - X->x * s;
  Y->x = Y_x;
}
//---------------------------------------------------------------------------
// поворот вектора X вокруг оси OZ на угол A, результат помещается в Y
void fly_vector_rotate_oz(double A, const fly_3d *X, fly_3d *Y)
{
  double c = cos(A);
  double s = sin(A);
  double Y_x;
  
  // матрица поворота вокруг оси OZ:
  // [ cos(A) -sin(A) 0 ]
  // [ sin(A)  cos(A) 0 ]
  // [   0       0    1 ]
  Y_x  = X->x * c - X->y * s;
  Y->y = X->x * s + X->y * c;
  Y->z = X->z;
  Y->x = Y_x;
}
//---------------------------------------------------------------------------
// Функция вычисления приращения вращающегося вектора
// FIXME: возможно можно оптимизировать используя кватернионы
void fly_vector_rotate(
  double       dt,   // время вращения [сек] 
  const fly_3d *o,   // вектор угловой скорости вращения [рад/с]
  const fly_3d *x,   // исходный вектор
  fly_3d       *dx)  // результат: приращение вектора x за заданное время dt
{
  fly_3d v, y, z;
  fly_3x3 m;
  double v_abs;

  double o_abs = fly_module(o); // модуль угловой скорости [рад/с]
  fly_vector_mpy(o, x, &v);     // вектор скорости вектора
  v_abs = fly_module(&v);       // модуль скорости вектора

  if (v_abs == 0.)
  { // если скорость равна нулю, то возвращаем нулевое приращение
    dx->x = dx->y = dx->z = 0.;
    return;
  }

  // определить "вращательную" декартову систему координат (ВСК):
  //   ось OZ коллинеарна вектору угловой скорости
  //   ось OY коллинеарна вектору линейной скорости
  //   ось OX дополняет систему до правой декартовой
  // создать матрицу перехода от вращательной СК (ВСК) к данной (m)
  // т.е. определить координаты базисных векторов ВСК в данной СК
  fly_vector_norm_copy(o,  &m.k);
  fly_vector_norm_copy(&v, &m.j);
  fly_vector_mpy(&m.j, &m.k, &m.i);

  // вычислить координаты вращаемого вектора в ВСК
  fly_vector_reverse(&m, x, &y);

  // произвести поворот вектора на заданный угол вокруг оси OZ в ВСК
  fly_vector_rotate_oz(o_abs * dt, &y, &z);

  // преобразовать координаты повернутого вектора в исходную СК
  fly_vector_transform(&m, &z, &y);

  // вычислить и вернуть линейное приращение
  dx->x = y.x - x->x;
  dx->y = y.y - x->y;
  dx->z = y.z - x->z;
}
//---------------------------------------------------------------------------
// поворот базисных векторов матрицы перехода относительно одной из осей
void fly_matrix_rotate_one(
  int axis,          // идентификатор оси (см. FLY_ROTATE_xx)
  double alpha,      // угол поворота [рад]
  const fly_3x3 *ma, // входная матрица перехода
  fly_3x3 *mb)       // выходная матрица перехода
{
  if (axis == FLY_ROTATE_OX)
  { // повотот вокруг оси OX
    fly_vector_rotate_ox(alpha, &ma->i, &mb->i);
    fly_vector_rotate_ox(alpha, &ma->j, &mb->j);
    fly_vector_rotate_ox(alpha, &ma->k, &mb->k);
  }
  else if (axis == FLY_ROTATE_OY)
  { // повотот вокруг оси OY
    fly_vector_rotate_oy(alpha, &ma->i, &mb->i);
    fly_vector_rotate_oy(alpha, &ma->j, &mb->j);
    fly_vector_rotate_oy(alpha, &ma->k, &mb->k);
  }
  else // axis == FLY_ROTATE_OZ
  { // повотот вокруг оси OZ
    fly_vector_rotate_oz(alpha, &ma->i, &mb->i);
    fly_vector_rotate_oz(alpha, &ma->j, &mb->j);
    fly_vector_rotate_oz(alpha, &ma->k, &mb->k);
  }
}
//---------------------------------------------------------------------------
// поворот базисных векторов матрицы перехода по трем осям
//
// пример вызова (формирование матрицы перехода от ССК к ГСК):
//   fly_get_me(&me); // формирование единичной матрицы
//   fly_matrix_rotate(
//     FLY_ROTATE_OX, FLY_ROTATE_OZ, FLY_ROTATE_OY, // порядок поворотов
//     gamma,   // крен (OX)
//     teta,    // тангаж (OZ)
//     -psi,    // курс (OY)
//     &me,     // единичная матрица
//     &msg);   // матрица перехода от ССК к ГСК
void fly_matrix_rotate(
  int axis1,         // идентификатор поворота #1 (см. FLY_ROTATE_xx)
  int axis2,         // идентификатор поворота #2 (см. FLY_ROTATE_xx)
  int axis3,         // идентификатор поворота #3 (см. FLY_ROTATE_xx)
  double alpha1,     // угол поворота #1 [рад]
  double alpha2,     // угол поворота #2 [рад]
  double alpha3,     // угол поворота #3 [рад]
  const fly_3x3 *ma, // входная матрица перехода
  fly_3x3 *mb)       // выходная матрица перехода
{
  fly_matrix_rotate_one(axis1, alpha1, ma, mb);
  fly_matrix_rotate_one(axis2, alpha2, mb, mb);
  fly_matrix_rotate_one(axis3, alpha3, mb, mb);
}
//---------------------------------------------------------------------------
// функция перемножения матриц 3x3
// C = A x B
void fly_matrix_mpy(const fly_3x3 *A, const fly_3x3 *B, fly_3x3 *C)
{
  fly_vector_transform(A, &B->i, &C->i);
  fly_vector_transform(A, &B->j, &C->j);
  fly_vector_transform(A, &B->k, &C->k);
}
//---------------------------------------------------------------------------
// функция перехода от одного ортонормированного базиса к другому
// A - координаты базисных векторов первой СК во второй (вход)
// B - координаты базисных векторов второй СК в первой (результат)
// выполняется транспонирование матриы 3x3
void fly_transp3x3(const fly_3x3 *A, fly_3x3 *B)
{
  // необходимо транспонировать матрицу перехода
  //     [ i.x  j.x  k.x ]
  //     [               ]
  // A = [ i.y  j.y  k.y ]
  //     [               ]
  //     [ i.z  j.z  k.z ]
  B->i.x = A->i.x; B->i.y = A->j.x; B->i.z = A->k.x;
  B->j.x = A->i.y; B->j.y = A->j.y; B->j.z = A->k.y;
  B->k.x = A->i.z; B->k.y = A->j.z; B->k.z = A->k.z;
}
//---------------------------------------------------------------------------
// вычисление определителя матрицы 3x3
double fly_det_3x3(const fly_3x3 *A)
{
  return FLY_DET_3X3(A->i.x, A->j.x, A->k.x,
                     A->i.y, A->j.y, A->k.y,
                     A->i.z, A->j.z, A->k.z);
}  
//---------------------------------------------------------------------------
// вычисление определителя матрицы 4x4
double fly_det_4x4(double a11, double a12, double a13, double a14,
                   double a21, double a22, double a23, double a24,
                   double a31, double a32, double a33, double a34,
                   double a41, double a42, double a43, double a44)
{
  return a11 * FLY_DET_3X3(a22, a23, a24,
                           a32, a33, a34,
                           a42, a43, a44) -
         a12 * FLY_DET_3X3(a21, a23, a24,
                           a31, a33, a34,
                           a41, a43, a44) +
         a13 * FLY_DET_3X3(a21, a22, a24,
                           a31, a32, a34,
                           a41, a42, a44) -
         a14 * FLY_DET_3X3(a21, a22, a23,
                           a31, a32, a33,
                           a41, a42, a43);
}
//---------------------------------------------------------------------------
// функция решения системы из 3-х линейных уравнений с тремя неизвестными
// методом Крамера (полагаем B = A x X),
// функция возвращается детерминант det|A|,
// если нет единственного решения (det|A| = 0), результат X заполняется 0
double fly_solve_3d(
  const fly_3x3 *A, // матрица 3x3 коэффициентов при неизвестных
  const fly_3d *B,  // столбец правых частей
  fly_3d *X)        // результат
{
  // определитель матрици A
  double a = FLY_DET_3X3(A->i.x, A->j.x, A->k.x,
                         A->i.y, A->j.y, A->k.y,
                         A->i.z, A->j.z, A->k.z);
  if (a != 0.)
  { // единственное решение есть
    X->x = FLY_DET_3X3(B->x, A->j.x, A->k.x,
                       B->y, A->j.y, A->k.y,
                       B->z, A->j.z, A->k.z) / a;
    
    X->y = FLY_DET_3X3(A->i.x, B->x, A->k.x,
                       A->i.y, B->y, A->k.y,
                       A->i.z, B->z, A->k.z) / a;
    
    X->z = FLY_DET_3X3(A->i.x, A->j.x, B->x,
                       A->i.y, A->j.y, B->y,
                       A->i.z, A->j.z, B->z) / a;
  }
  else
  { // нет единственного решения
    X->x = X->y = X->z = 0.;
  }
  return a;
}
//---------------------------------------------------------------------------
// функция решения системы из 4-х линейных уравнений с четырьмя неизвестными
// методом Крамера (полагаем B = A x X),
// функция возвращается детерминант det|A|,
// если нет единственного решения (det|A| = 0), результат X заполняется 0
double fly_solve_4d(
  const double *A, // матрица 4x4 коэффициентов при неизвестных [16]
  const double *B, // столбец правых частей [4]
  double *X)       // результат [4]
{
  // определитель матрици A
  double a = fly_det_4x4(A[ 0], A[ 1], A[2],  A[ 3],
                         A[ 4], A[ 5], A[6],  A[ 7],
                         A[ 8], A[ 9], A[10], A[11],
                         A[12], A[13], A[14], A[15]);
  if (a != 0.)
  { // единственное решение есть
    X[0] = fly_det_4x4(B[ 0], A[ 1], A[ 2], A[ 3],
                       B[ 1], A[ 5], A[ 6], A[ 7],
                       B[ 2], A[ 9], A[10], A[11],
                       B[ 3], A[13], A[14], A[15]) / a;
    
    X[1] = fly_det_4x4(A[ 0], B[ 0], A[ 2], A[ 3],
                       A[ 4], B[ 1], A[ 6], A[ 7],
                       A[ 8], B[ 2], A[10], A[11],
                       A[12], B[ 3], A[14], A[15]) / a;
    
    X[2] = fly_det_4x4(A[ 0], A[ 1], B[ 0], A[ 3],
                       A[ 4], A[ 5], B[ 1], A[ 7],
                       A[ 8], A[ 9], B[ 2], A[11],
                       A[12], A[13], B[ 3], A[15]) / a;
    
    X[3] = fly_det_4x4(A[ 0], A[ 1], A[2],  B[ 0],
                       A[ 4], A[ 5], A[6],  B[ 1],
                       A[ 8], A[ 9], A[10], B[ 2],
                       A[12], A[13], A[14], B[ 3]) / a;
  }
  else
  { // нет единственного решения
    X[0] = X[1] = X[2] = X[3] = 0.;
  }
  return a;
}
//---------------------------------------------------------------------------
// функция перехода от одного базиса к другому
// A - координаты базисных векторов первой СК во второй (вход)
// B - координаты базисных векторов второй СК в первой (результат)
// выполняется обращение матрицы 3x3
void fly_invert3x3(const fly_3x3 *A, fly_3x3 *B)
{
  double det;
  // необходимо обратить матрицу перехода
  //     [ i.x  j.x  k.x ]
  //     [               ]
  // A = [ i.y  j.y  k.y ]
  //     [               ]
  //     [ i.z  j.z  k.z ]

  det = fly_det_3x3(A); // вычисляем определитель det(A)
  
  B->i.x = (A->j.y * A->k.z - A->j.z * A->k.y) / det;
  B->j.x = (A->j.z * A->k.x - A->j.x * A->k.z) / det;
  B->k.x = (A->j.x * A->k.y - A->j.y * A->k.x) / det;
  
  B->i.y = (A->i.z * A->k.y - A->i.y * A->k.z) / det;
  B->j.y = (A->i.x * A->k.z - A->i.z * A->k.x) / det;
  B->k.y = (A->i.y * A->k.x - A->i.x * A->k.y) / det;
  
  B->i.z = (A->i.y * A->j.z - A->i.z * A->j.y) / det;
  B->j.z = (A->i.z * A->j.x - A->i.x * A->j.z) / det;
  B->k.z = (A->i.x * A->j.y - A->i.y * A->j.x) / det;
}
//---------------------------------------------------------------------------
// функция расчета Эйлеровых углов из координат базисных векторов ГСК в ССК
// (mgs - матрица перехода от ГСК к ССК)
void fly_mgs2e(const fly_3x3 *mgs, fly_angles *e)
{
  double s, c, ct; // sin(...), cos(...), cos(teta)
  
  // матрица перехода от ГСК к CСК (mgs)
  // "Обработка результатов испытаний" М.И.Хейфец Машиностр. 1988г (стр. 24)
  // Значение курса (psi) возвращается с обратным знаком угла рысканья!
  // [ i.x j.x k.x ]   [ cp*ct            st       -sp*ct      ]
  // [             ]   [                                       ]
  // [ i.y j.y k.y ] = [ sp*sg-cp*st*cg  ct*cg  cp*sg+sp*st*cg ]
  // [             ]   [                                       ]
  // [ i.z j.z k.z ]   [ sp*cg+cp*st*sg -ct*sg  cp*cg-sp*st*sg ]
  // где cp = cos(psi),  sp = sin(psi),
  //     ct = cos(teta), st = sin(teta),
  //     cg = cos(gama), sg = sin(gama)
  
  // "тангаж"
  s  = mgs->j.x;          // sin(teta)
  ct = sqrt(1. - s * s);  // cos(teta)
  e->teta = atan2(s, ct); // [-pi/2, pi/2]
  
  if (ct == 0.)
  {
    // если "тангаж" +/- 90 градусов придется немного извернуться...
    // определена лишь сумма "крен" + "курс"
    // считаем, что "крен" равен нулю, чтобы обнулить поправку СУУ по крену
    // и вычисляем только курс
    e->gama = 0.;
    s = mgs->k.y; // sin(psi)
    c = mgs->k.z; // cos(psi)
    e->psi = -atan2(s, c); // [-pi, pi] возвращаем курс, а не угол рысканья!
    return;
  }
  
  // "курс"
  s = - mgs->k.x / ct; // sin(psi)
  c =   mgs->i.x / ct; // cos(psi)
  e->psi = -atan2(s, c); // [-pi, pi] возвращаем курс, а не угол рысканья!
    
  // "крен"
  s = - mgs->j.z / ct; // sin(gama)
  c =   mgs->j.y / ct; // cos(gama)
  e->gama = atan2(s, c); // [-pi, pi]
}
//---------------------------------------------------------------------------
// функция расчета базисных векторов ГСК в ССК по значениям Эйлеровых углов
// (mgs - матрица перехода от ГСК к ССК)
void fly_e2mgs(const fly_angles *e, fly_3x3 *mgs)
{
  double cos_psi  = cos(e->psi);
  double sin_psi  = -sin(e->psi); // используем курс, а не угол рысканья
  double cos_gama = cos(e->gama);
  double sin_gama = sin(e->gama);
  double cos_teta = cos(e->teta);
  double sin_teta = sin(e->teta);
  
  // матрица перехода от ГСК к CСК (mgs)
  // "Обработка результатов испытаний" М.И.Хейфец Машиностр. 1988г (стр. 24)
  // Значение курса (psi) преобразуется с обратным знаком к углу рысканья!
  // [ i.x j.x k.x ]   [ cp*ct            st       -sp*ct      ]
  // [             ]   [                                       ]
  // [ i.y j.y k.y ] = [ sp*sg-cp*st*cg  ct*cg  cp*sg+sp*st*cg ]
  // [             ]   [                                       ]
  // [ i.z j.z k.z ]   [ sp*cg+cp*st*sg -ct*sg  cp*cg-sp*st*sg ]
  // где cp = cos(psi),  sp = sin(psi),
  //     ct = cos(teta), st = sin(teta),
  //     cg = cos(gama), sg = sin(gama)
  mgs->i.x = cos_teta * cos_psi;
  mgs->j.x = sin_teta;
  mgs->k.x = - cos_teta * sin_psi;
  mgs->i.y = sin_gama * sin_psi - cos_gama * sin_teta * cos_psi;
  mgs->j.y = cos_gama * cos_teta;
  mgs->k.y = cos_gama * sin_teta * sin_psi + sin_gama * cos_psi;
  mgs->i.z = cos_gama * sin_psi + sin_gama * sin_teta * cos_psi;
  mgs->j.z = - sin_gama * cos_teta;
  mgs->k.z = cos_gama * cos_psi - sin_gama * sin_teta * sin_psi;
}
//---------------------------------------------------------------------------
// функция пересчета координат из ИСК в ЗСК
void fly_xi2xo(double t, const fly_3d *xi, fly_3d *xo)
{
  double r;
  // расчет геоцентрических декартовых координат
  //   матрица перехода от ИСК к ЗСК (mio):
  //     [   cos(FLY_OZ * t)  sin(FLY_OZ * t)  0 ]   [  t   r   0 ]
  //     [                                       ]   [            ]
  //     [ - sin(FLY_OZ * t)  cos(FLY_OZ * t)  0 ] = [ -r   t   0 ]
  //     [                                       ]   [            ]
  //     [          0                0         1 ]   [  0   0   1 ]
  t *= FLY_OZ; // угол поворота планеты вокруг своей оси за время полета
  r = sin(t); t = cos(t);
  xo->x = t * xi->x + r * xi->y;
  xo->y = t * xi->y - r * xi->x;
  xo->z = xi->z;
}
//---------------------------------------------------------------------------
// функция пересчета координат из ЗСК в ИСК
void fly_xo2xi(double t, const fly_3d *xo, fly_3d *xi)
{
  double r;
  // расчет декартовых координат в ИСК
  //   матрица перехода от ЗСК к ИСК (moi):
  //     [ cos(FLY_OZ * t)  -sin(FLY_OZ * t)  0 ]   [ t  -r   0 ]
  //     [                                      ]   [           ]
  //     [ sin(FLY_OZ * t)   cos(FLY_OZ * t)  0 ] = [ r   t   0 ]
  //     [                                      ]   [           ]
  //     [        0                 0         1 ]   [ 0   0   1 ]
  t *= FLY_OZ; // угол поворота планеты вокруг своей оси за время полета
  r = sin(t); t = cos(t);
  xi->x = t * xo->x - r * xo->y;
  xi->y = t * xo->y + r * xo->x;
  xi->z = xo->z;
}
//---------------------------------------------------------------------------
// функция расчета базисных векторов скоростной СК в ССК по значению 
// воздушной скорости в ССК
// (mvs - матрица перехода от скоростной СК к ССК)
void fly_vs2mvs(const fly_3d *vs, fly_3x3 *mvs)
{
  double r = fly_module(vs);
  mvs->i.x = vs->x / r;
  mvs->i.y = vs->y / r;
  mvs->i.z = vs->z / r;

  // полагаем, что угол скольжения менее 90 градусов
  mvs->k.x = 0.;
  mvs->k.y = 0.;
  mvs->k.z = 1.;
  fly_vector_mpy(&mvs->k, &mvs->i, &mvs->j);
  r = fly_module(&mvs->j);
  mvs->j.x = mvs->j.x / r;
  mvs->j.y = mvs->j.y / r;
  mvs->j.z = mvs->j.z / r;
  fly_vector_mpy(&mvs->i, &mvs->j, &mvs->k);
}
//---------------------------------------------------------------------------
// функция решения квадратного уравнения: a * x^2 + b * x + c = 0
// (функция возвращяет число решений: 0, 1 или 2)
int fly_solve_square(double a, double b, double c, double *x1, double *x2)
{
  double d = b * b - 4. * a * c;
  if (d < 0.)
  { // решений нет
    *x1 = *x2 = 0.;
    return 0;
  }
  a *= 2.;
  
  if (d == 0.)
  { // есть одно решение
    *x1 = *x2 = -b / a;
    return 1;
  }
  else // d > 0.
  { // есть два решения
    d = sqrt(d);
    *x1 = (-b - d) / a;
    *x2 = (-b + d) / a;
    return 2;
  }
}
//---------------------------------------------------------------------------
// функция решения квадратного уравнения: a * x^2 + 2 * k * x + c = 0
// (функция возвращяет число решений: 0, 1 или 2)
int fly_solve_square2(double a, double k, double c, double *x1, double *x2)
{
  double d = k * k - a * c;
  if (d < 0.)
  { // решений нет
    *x1 = *x2 = 0.;
    return 0;
  }
  
  if (d == 0.)
  { // есть одно решение
    *x1 = *x2 = -k / a;
    return 1;
  }
  else // d > 0.
  { // есть два решения
    d = sqrt(d);
    *x1 = (-k - d) / a;
    *x2 = (-k + d) / a;
    return 2;
  }
}
//---------------------------------------------------------------------------
// функция расчета вектора нормального к поверхности планеты и проходящего
// через заданную точку в геоцентрической СК
// (функция возвращает абсолютную высоту заданной точки над поверхностью)
double fly_xo2h(
  const fly_3d *xo,  // заданная точка в ЗСК
  fly_3d *n)         // результат - единичный вектор нормали
{
#if 1 // вариант 2019.03.21
  fly_3d x;
  double a, h;
  double k = fly_bz / fly_az;
  double k2 = k * k;
  
  // n - единичный вектор нормальный к поверхности "надутой" планеты
  // (первая итерация "пристрелочная")
  n->x = xo->x * k;
  n->y = xo->y * k;
  n->z = xo->z / k;
  a = FLY_MODULE(n->x, n->y, n->z);
  n->x /= a;
  n->y /= a;
  n->z /= a;

  // x - вектор от поверхности эллипсоида до заданной точки
  a = fly_az / FLY_MODULE(n->x, n->y, n->z * k);
  x.x = xo->x - n->x * a;
  x.y = xo->y - n->y * a;
  x.z = xo->z - n->z * a * k2;

  // h - абсолютная высота (расстояние от эллипсоида до заданной точки)
  h = x.x * n->x + x.y * n->y + x.z * n->z;
  
  // x - вектор из центра планеты до поверхности эллипсоида
  // (подготовка ко второй итерации)
  x.x = xo->x - n->x * h;
  x.y = xo->y - n->y * h;
  x.z = xo->z - n->z * h;

  // n - единичный вектор нормальный к поверхности планеты
  // (вторая итерация)
  n->x = x.x * k;
  n->y = x.y * k;
  n->z = x.z / k;
  a = FLY_MODULE(n->x, n->y, n->z);
  n->x /= a;
  n->y /= a;
  n->z /= a;

  // x - вектор от поверхности эллипсоида до заданной точки
  a = fly_az / FLY_MODULE(n->x, n->y, n->z * k);
  x.x = xo->x - n->x * a;
  x.y = xo->y - n->y * a;
  x.z = xo->z - n->z * a * k2;

  // h - абсолютная высота (расстояние от эллипсоида до заданной точки)
  h = x.x * n->x + x.y * n->y + x.z * n->z;
  
  // x - вектор из центра планеты до поверхности эллипсоида
  // (подготовка к третей итерации)
  x.x = xo->x - n->x * h;
  x.y = xo->y - n->y * h;
  x.z = xo->z - n->z * h;

  // n - единичный вектор нормальный к поверхности планеты
  // (третья итерация по уточнению нормали, высоту больше не уточняем)
  n->x = x.x * k;
  n->y = x.y * k;
  n->z = x.z / k;
  a = FLY_MODULE(n->x, n->y, n->z);
  n->x /= a;
  n->y /= a;
  n->z /= a;

  // вернуть абсолютную высоту (расстояние от эллипсоида до заданной точки)
  return h;

#else // вариант 2005..2011 гг, содержит грубые ошибки
  double a = FLY_MODULE(xo->x, xo->y, xo->z);
  double d = (a - FLY_MODULE(xo->x / a * fly_az,
                             xo->y / a * fly_az,
                             xo->z / a * fly_bz)) / a;

  // n - единичный вектор нормальный к поверхности планеты
  n->x = xo->x * fly_bz;
  n->y = xo->y * fly_bz;
  n->z = xo->z * fly_az;
  a = FLY_MODULE(n->x, n->y, n->z);
  n->x /= a;
  n->y /= a;
  n->z /= a;

  // вернуть абсолютную высоту (расстояние от эллипсоида до заданной точки)
  return (xo->x * n->x + xo->y * n->y + xo->z * n->z) * d;
#endif
}
//---------------------------------------------------------------------------
// обратная функция к fly_xo2h()
void fly_h2xo(
  double h,        // абсолютная высота
  const fly_3d *n, // вектор нормали к поверхности планеты
  fly_3d *xo)      // результат - геоцентрическая координата заданной точки
{
#if 1 // вариант 2019.03.21
  double k = fly_bz / fly_az;
  double a = fly_az / FLY_MODULE(n->x, n->y, n->z * k);

  // xo - радиус вектор заданной точки
  xo->x = n->x * (h + a);
  xo->y = n->y * (h + a);
  xo->z = n->z * (h + a * k * k);

#else // вариант 2005..2011 гг, содержит грубые ошибки
  // xo - единичный вектор из центра планеты
  xo->x = n->x * fly_az;
  xo->y = n->y * fly_az;
  xo->z = n->z * fly_bz;
  a = FLY_MODULE(xo->x, xo->y, xo->z);
  xo->x /= a;
  xo->y /= a;
  xo->z /= a;

  // модуль радиус вектора из центра земли
  a = FLY_MODULE(xo->x * fly_az, xo->y * fly_az, xo->z * fly_bz) + 
      h / (xo->x * m.x + xo->y * m.y + xo->z * m.z);

  // возвращаем радиус вектор из центра земли до заданной точки
  xo->x *= a;
  xo->y *= a;
  xo->z *= a;
#endif
}
//---------------------------------------------------------------------------
// функция расчета матрицы перехода (mno) от НСК к ЗСК
// по координатам центра масс ЛА в ЗСК
// ИЛИ матрицы перехода (mno) от НСК к ИСК
// по координатам центра масс ЛА в ИСК
void fly_xo2mno(const fly_3d *x, fly_3x3 *mno)
{
  double a, b, c;
  // базисный вектор "j" ГСК в ЗСК (направлен от центра земли) 
      mno->j.x = x->x / (a = sqrt((c = x->x * x->x + x->y * x->y) + 
                                  x->z * x->z));
      mno->j.y = x->y / a;
  a = mno->j.z = x->z / a;  // a = sin("геоцентрической широты")
  b = x->x / (c = sqrt(c)); // b = cos("долготы")
  c = x->y / c;             // c = sin("долготы")
  
  // базисный вектор "i" ГСК в ЗСК (направлен на Север) 
  mno->i.x = - b * a; // - cos("долготы") * sin("геоцентрической широты")
  mno->i.y = - c * a; // - sin("долготы") * sin("геоцентрической широты")
  mno->i.z = sqrt(1. - a * a); //           cos("геоцентрической широты")
    
  // базисный вектор "k" ГСК в ЗСК (направлен на Восток)
  fly_vector_mpy(&mno->i, &mno->j, &mno->k);
}
//---------------------------------------------------------------------------
// функция расчета матрицы перехода (mgo) от ГСК к ЗСК
// по координатам центра масс ЛА в ЗСК
// ИЛИ матрицы перехода (mgi) от ГСК к ИСК
// по координатам центра масс ЛА в ИСК
void fly_xo2mgo(const fly_3d *xo, fly_3x3 *mgo)
{
  fly_3d n;
  fly_xo2h(xo, &n); // n - вектор нормали к поверхности планеты
  fly_xo2mno(&n, mgo); // используем отлаженную процедуру
}
//---------------------------------------------------------------------------
// функция пересчета декартовых координат центра масс ЛА в геоцентрической СК
// в полярные координаты в геоцентрической СК
void fly_xo2geoc(const fly_3d *xo, fly_geoc *geoc)
{
  double r;
  // расчет полярных координаты из декартовых
  geoc->lambda = atan2(xo->y, xo->x);
  geoc->fi = atan2(xo->z, sqrt(r = xo->x * xo->x + xo->y * xo->y));
  geoc->r = sqrt(r + xo->z * xo->z);
}
//---------------------------------------------------------------------------
// функция пересчета полярных координат центра масс ЛА в геоцентрической СК
// в декартовые координаты в геоцентрической СК
void fly_geoc2xo(const fly_geoc *geoc, fly_3d *xo)
{
  double r;
  // расчет геоцентрических декартовых координат из полярных
  r     = geoc->r * cos(geoc->fi);
  xo->z = geoc->r * sin(geoc->fi);
  xo->x = r * cos(geoc->lambda);
  xo->y = r * sin(geoc->lambda);
}
//---------------------------------------------------------------------------
// функция пересчета геоцентрических координат в географические
void fly_xo2geog(const fly_3d *xo, fly_geog *geog)
{
  fly_3d n; // нормаль к поверхности эллипсоида
  geog->h = fly_xo2h(xo, &n); // абсолютная высота
  geog->lambda = atan2(n.y, n.x);
  geog->fi = atan2(n.z, sqrt(n.x * n.x + n.y * n.y));
}
//---------------------------------------------------------------------------
// функция пересчета географических координат в геоцентрические
void fly_geog2xo(const fly_geog *geog, fly_3d *xo)
{
  double r;
  fly_3d n; // нормаль к поверхности эллипсоида
  r   = cos(geog->fi);
  n.z = sin(geog->fi);
  n.x = r * cos(geog->lambda);
  n.y = r * sin(geog->lambda);
  fly_h2xo(geog->h, &n, xo);
}
//---------------------------------------------------------------------------
// функция расчета радиуса земли от долготы и геоцентрической широты
// "уровень мирового океана"
double fly_rz(double lambda, double fic)
{
  // используем (как и ранее) аппроксимацию референц-эллипсоидом
  double a2 = fly_az * fly_az;
  double b2 = fly_bz * fly_bz;
  double s = sin(fic);
  double c = cos(fic);
  double s2 = s * s;
  double c2 = c * c;
  return sqrt(a2 * b2 / (a2 * s2 + b2 * c2));
}
//---------------------------------------------------------------------------
// приближенное вычисление географической широты из геоцентрической
double fly_fig(double fic)
{
  // на геоцентрической широте 45 градусов географическая широты
  // больше геоцентрической широты на 11.5438 минут
  return fic + FLY_KZ * sin(2. * fic);
}
//---------------------------------------------------------------------------
// приближенное вычисление геоцентрической широты из географической
double fly_fic(double fig)
{
  // на геоцентрической широте 45 градусов географическая широты
  // больше геоцентрической широты на 11.5438 минут
  double dif = FLY_KZ * sin(2. * fig);         // первая итерация
         dif = FLY_KZ * sin(2. * (fig - dif)); // втрая итерация
         dif = FLY_KZ * sin(2. * (fig - dif)); // третя итерация
  return fig - FLY_KZ * sin(2. * (fig - dif)); // четвертая итерация
}
//---------------------------------------------------------------------------
// приведение времени к диапазону суточного времени
// функция возвращает значение в диаппазоне [0, FLY_TZ)
// и время в целом числе суток
int fly_day_time(
  double t,   // время в любом диапазоне
  double *pt) // время суток в диапазоне [0, FLY_TZ)
{
 int days = 0;
 if (t < 0.)
 {
   while (t < 0.)
   {
     t += FLY_TZ;
     days--;
   }
 }
 else // t >= 0
 {
   while (t >= FLY_TZ)
   {
     t -= FLY_TZ;
     days++;
   }
 }
 if (pt != (double*) NULL) *pt = t;
 return days;
}
//---------------------------------------------------------------------------
// Модель стандартной атмосферы
//   вход: h - высота относительно местного радиуса планеты [м]
//  выход: p - плотность воздуха [кг/м^3]
//         a - скорость звука [м/с]
//         P - атмосферное давление [Па]
//         T - температура [К]
// см. "Обработка результатов испытаний" М.И.Хейфец стр. 51-54
void fly_atmosphere(double h, double *p, double *a, double *P, double *T)
{
#ifdef FLY_FUNC_ISO
  // изотермическая модель атмосферы
  // примерно (до 5%) соответсвует стандартной в диаппазоне -2...7 км
  *p = FLY_RO0 * exp(-0.000098 * h);
  *a = sqrt(1.400 * 287.053 * 288.150); // sqrt(k*R*T)
  *P = p * (287.053 * 288.150); // p * (R*T)
  *T = 288.150;
#else
  double r = 6.356766e6; // условный радиус земли [м]
  double R = 287.053;    // газовая постоянная удельная (Rс) [Дж/(кг*К)]
  double g = 9.80665;    // стандартное ускорение свободного падения [м/с^2]
  double k = 1.400;      // отношение удельных теплоемкостей Cp/Cv
  double Hs, H;          // геопотенциальная высота [м] (Hs - нач. интервала)
  double b;              // градиент температуры [К/м]
  double Ts;             // абсолютная температура на нач. интервала [К]
  double Ps;             // статическое давление на нач. интервала [Па]

  // вычислим геопотенциальную высоту
  H = r * h / (r + h); // [м]
  
  // выбор интервала
  // -2...11 км
  if      (H < 11e3){ b = -6.5e-3;  Ts = 301.15; Hs = -2e3; Ps = 1.27774e5; }
  // 11...20 км
  else if (H < 20e3){ b = 0.;       Ts = 216.65; Hs = 11e3; Ps = 2.26321e4; }
  // 20...32 км
  else if (H < 32e3){ b = 1e-3;     Ts = 216.65; Hs = 20e3; Ps = 5.47489e3; }
  // 32...47 км
  else if (H < 47e3){ b = 2.8e-3;   Ts = 228.65; Hs = 32e3; Ps = 8.68018e2; }
  // 47...51 км
  else if (H < 51e3){ b = 0.0;      Ts = 270.65; Hs = 47e3; Ps = 1.10906e2; }
  // 51...71 км
  else              { b = -2.8e-3;  Ts = 270.65; Hs = 51e3; Ps = 6.69387e1; }
  
  // вычислим температуру и давление
  if (b != 0.)
  {
    *T = Ts + b * (H - Hs);
    *P = Ps * pow(1. + b / Ts * (H - Hs), -g / (b * R));
  }
  else // b == 0
  {
    *T = Ts;
    *P = Ps * exp(-g / (R * Ts) * (H - Hs));
  }
  
  // вычислим плотность
  *p = (*P) / (R * (*T));

  // вычислим скорость звука
  *a = sqrt(k * R * (*T));
#endif
}
//---------------------------------------------------------------------------
// вернуть глобальный радиус референц-эллипсоида на экваторе [м]
double fly_get_az() { return fly_az; }
//---------------------------------------------------------------------------
// установить глобальный радиус референц-эллипсоида на экваторе [м]
void fly_set_az(double az) { fly_az = az; }
//---------------------------------------------------------------------------
// вернуть глобальный радиус референц-эллипсоида на полюсах [м]
double fly_get_bz() { return fly_bz; }
//---------------------------------------------------------------------------
// установить глобальный радиус референц-эллипсоида на полюсах [м]
void fly_set_bz(double bz) { fly_bz = bz; }
//---------------------------------------------------------------------------
// установить систему СК-42 как текущую
void fly_set_sk42()
{
  fly_set_az(FLY_AZ_SK42);
  fly_set_bz(FLY_BZ_SK42);
}
//---------------------------------------------------------------------------
// установить систему WGS-84 как текущую
void fly_set_wgs84()
{
  fly_set_az(FLY_AZ_WGS84);
  fly_set_bz(FLY_BZ_WGS84);
}
//---------------------------------------------------------------------------

/*** end of "fly_lib.c" file ***/

